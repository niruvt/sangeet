module           = "sangeet"
typesetexe       = "lualatex"
typesetsuppfiles = { "gfdl-tex.tex" }
typesetfiles     = { "sangeet.dtx" }
docfiles         = { "COPYING" }
typesetruns      = 2
